﻿<?php
    require_once('stateMachineDemo.php');
    if (!isset($_GET['id']))
        die("Δεν έχει οριστεί το αναγνωριστικό της δραστηριότητας");
    
    if (!file_exists('activities/' . $_GET['id'] . ".json"))
        die("Δεν υπάρχει δραστηριότητα με αυτό το αναγνωριστικό");

    $string = file_get_contents("activities/" . $_GET['id'] . ".json");
    $activity = json_decode($string, true);

    $nodes = array();
    $cnt=0;
    foreach ($activity['nodes'] as $line) {
        $nodes[$cnt++] = $line;
    }
    shuffle($nodes);
    
    $title = $activity['metadata']['title'];
    $description = $activity['metadata']['subtitle'];
?>

<!doctype html>
<html>
	<head>
		<title><?php echo $title; ?></title>
		<link rel="stylesheet" href="./css/jsPlumbDemo.css">
        
        <style>
            <?php stateMachineDemoCSS($nodes); ?>
        </style>
		
        <link rel="stylesheet" href="./ibox.2.2/ibox.css" type="text/css" media="screen"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <script type="text/javascript" src="ibox.2.2/ibox.js"></script>
    </head>

	<body data-demo-id="stateMachineDemo" data-library="jquery" style="background-image:url(images/bg2.png)">
    
        <div id="inner_content" style="display:none; height:150px;">
            <div>
                <ul>
                    <li><font face="Verdana" size="2">Για να κάνετε τις συνδέσεις πατήστε στα 
                        <font color="#0033cc"><b>μπλε</b></font> <font color="#0033cc"><b>κουτάκια</b></font> και μετά  σύρετε με το ποντίκι μέχρι το πλαίσιο που 
                        θέλετε (μόνο οι σωστές συνδέσεις θα αποτυπώνονται στον εννοιολογικό χάρτη)</font>
                    </li>
                    <li><font face="Verdana" size="2">Αν θέλετε να καταργήσετε μια σύνδεση, 
                        μπορείτε να κάνετε κλικ επάνω της.</font>
                    </li>
                    <li><font face="Verdana" size="2">Μπορείτε να μετακινήσετε όλα τα πλαίσια.</font>
                    </li>
                </ul>
                <p>&nbsp;</p>
            </div> 
        </div>
        
		<div style="position:absolute; top:23px; left:820px; width: 110px;">        
            <a href="dschInfo.html" target="_blank"><img src="images/info3.png" alt="" width="32" height="32" border="0"></a>
            <br />
            <a href="#inner_content" rel="ibox"><img src="images/help.png" width="32" height="32" border="0" align="absmiddle"> Βοήθεια</a>
        </div>
        
		<div style="position:absolute">
            <div id="demo">	
                <?php
                    for ($i=0; $i<count($nodes); $i++) {
                        echo '<div class="w" id="' . $nodes[$i]['id'] . '">' . $nodes[$i]['description'] . '<div class="ep"></div></div>';
                    }
                ?>   
            </div>
		</div>

        <div id="explanation">
            <p style="line-height: 150%; margin-top: 0; margin-bottom: 0"><font face="Verdana" size="2">
                <span id="title"><?php echo $description; ?></span>
                <br />
                <b>Σκορ: <span id="alx_correct">0</span> / <span id="alx_all">0</span></b>
                <br /> 
            </p>
        </div>
    
        <script type='text/javascript' src='js/jquery.min.js'></script>
        <script type='text/javascript' src='js/jquery-ui.min.js'></script>
        <script type='text/javascript' src='js/jquery.ui.touch-punch.min.js'></script>
        <script type='text/javascript' src='js/jquery.jsPlumb-1.3.16-all-min.js'></script>
        
        <script>
            <?php echo stateMachineDemoJS($nodes); ?>
        </script>
        
        <script type='text/javascript' src='js/stateMachineDemo-jquery.js'></script>
        <script type='text/javascript' src='js/demo-list.js'></script>
        <script type='text/javascript' src='js/demo-helper-jquery.js'></script>
        
    </body>
</html>
