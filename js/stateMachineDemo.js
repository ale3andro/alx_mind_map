;(function() {
	var curColourIndex = 1, maxColourIndex = 24, nextColour = function() {
		var R,G,B;
		R = parseInt(128+Math.sin((curColourIndex*3+0)*1.3)*128);
		G = parseInt(128+Math.sin((curColourIndex*3+1)*1.3)*128);
		B = parseInt(128+Math.sin((curColourIndex*3+2)*1.3)*128);
		curColourIndex = curColourIndex + 1;
		if (curColourIndex > maxColourIndex) curColourIndex = 1;
		return "rgb(" + R + "," + G + "," + B + ")";
	};
		
	window.jsPlumbDemo = { 
		init :function() {
			jsPlumb.importDefaults({
				Endpoint : ["Dot", {radius:2}],
				HoverPaintStyle : {strokeStyle:"#42a62c", lineWidth:2 },
				ConnectionOverlays : [
					[ "Arrow", { 
						location:1,
						id:"arrow",
	                    length:14,
	                    foldback:0.8
					} ],
	                [ "Label", { label:" ", id:"label" }] 
				]
			});

			jsPlumb.draggable(jsPlumb.getSelector(".w"));
			jsPlumb.bind("click", function(c) { 
				jsPlumb.detach(c); 
			});
				
			jsPlumbDemo.initEndpoints(nextColour);
            jsPlumb.bind("jsPlumbConnection", function(conn) {
                conn.connection.setPaintStyle({strokeStyle:nextColour()});
            });

            jsPlumb.makeTarget(jsPlumb.getSelector(".w"), {
				dropOptions:{ hoverClass:"dragHover" },
				anchor:"Continuous",
				beforeDrop:function(params) { 
					if ( 	( params.sourceId == "hy" &&  params.targetId == "software" ) || 
							( params.sourceId == "software" &&  params.targetId == "application_software" ) ||
							( params.sourceId == "application_software" &&  params.targetId == "paint" ) ||
							( params.sourceId == "application_software" &&  params.targetId == "word" ) ||
							( params.sourceId == "application_software" &&  params.targetId == "writer" ) ||
							( params.sourceId == "software" &&  params.targetId == "system_software" ) ||
							( params.sourceId == "system_software" &&  params.targetId == "macos" ) ||
							( params.sourceId == "system_software" &&  params.targetId == "win7" ) ||
							( params.sourceId == "hy" &&  params.targetId == "hardware" ) ||
							( params.sourceId == "hardware" &&  params.targetId == "monitor" )  ||
							( params.sourceId == "hardware" &&  params.targetId == "keyboard" )  ||
							( params.sourceId == "hardware" &&  params.targetId == "mouse" )  ||
							( params.sourceId == "hardware" &&  params.targetId == "printer" )  
						){
						return true;
					} else {
						return false;
					}
				}
			});
		}
	};
})();