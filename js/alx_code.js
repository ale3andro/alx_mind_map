/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}
// https://jsfiddle.net/Jonathan_Ironman/hbtq58m4/
function shuffleArray ( array ) {
    var counter = array.length, temp, index;
    // While there are elements in the array
    while ( counter > 0 ) {
        // Pick a random index
        index = Math.floor( Math.random() * counter );

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        temp = array[ counter ];
        array[ counter ] = array[ index ];
        array[ index ] = temp;
    }
    return array;
}

$( function() {
    if (!('id' in getUrlParams())) {
        $('#demo').html("Δεν δηλώθηκε το id της δραστηριότητας");
        return;
        /*
        $.ajax(
            {   url: "activities/all_activities",
                success: function(result) {
                    $('#alx_clause_title').html('Διαθέσιμες δραστηριότητες');
                    $('#alx_clause_subtitle').html('<ul>');
                    console.log(result);
                    var alldata = result.split('\n');
                    for (var i=0; i<alldata.length; i+=3) {
                        if (alldata[i]!='') {
                            var ttitle = alldata[i+1].substring(1, alldata[i+1].length-1);
                            var newLinePosition = ttitle.search('<br />');
                            if (newLinePosition!=-1)
                                ttitle=ttitle.substring(0, newLinePosition);
                            ttitle = "Τάξη " + alldata[i+2].substring(1, alldata[i+2].length-1) + " - " + ttitle;
                            $('#alx_clause_subtitle').html($('#alx_clause_subtitle').html() + "<li><a href='?id=" + alldata[i].substring(0, alldata[i].length-5) + "'>" + ttitle + "</a></li>");
                        }
                    }
                    $('#alx_clause_subtitle').html($('#alx_clause_subtitle').html() + '</ul>');
                    $('#alx_clauses').html('');
                },
                error: function(result) {
                    console.log('failure');
                } 
            });
        return; 
        */
    }

    var jqxhr = $.getJSON("activities/" + getUrlParams('id') + ".json", function(data) {
        title = data['metadata']['title'];
        subtitle = data['metadata']['subtitle'];
        $('#title').html(subtitle);
        $('#demo').html('');
        content = '';
        $.each(data['nodes'], function(index, value) {
            content += '<div class="w" id="' + value['id'] + '">' + value['description'] + '<div class="ep"></div></div>';
            
            //clauses.push(value['clause']);
            //answers.push(value['accepted_words']);
        });
        $('#demo').html(content);
        /*
        var full_text="";
        for (var i=0; i<clauses.length; i++) {
            full_text = full_text + "<p>" + i + ". " + clauses[i].replace("***", "<input id=\"textbox" + i + "\" type=\"text\">") + "</p>";
        }
        $('#alx_clauses').css('text-align', 'left');
        $('#alx_clauses').html(full_text + "<br /><button type=\"button\" onclick=\"checkAnswers();\" class=\"btn btn-primary\">Έλεγχος</button>");   
        $('#alx_clause_title').html(title);
        $('#alx_clause_subtitle').html(subtitle);   
        create_word_list(); 
        */
        //loadScripts();
        
    })
    .fail(function() {
        $('#demo').html('Μη διαθέσιμη δραστηριότητα...');
    });

    
    
    function loadScripts(num=0) {
        var scripts = ['js/jquery-ui.min.js', 'js/jquery.ui.touch-punch.min.js', 'js/jquery.jsPlumb-1.3.16-all-min.js', 'js/stateMachineDemo.js', 'js/stateMachineDemo-jquery.js', 'js/demo-list.js', 'js/demo-helper-jquery.js'];
        
        //$.getScript(scripts[i]).done(i)
        
    }


})
