<?php

    function stateMachineDemoCSS($nodes) {
        $numItemsPerColumn=3;
        $startX = 5;
        $startY = 5;
        echo "
            #demo {
                margin-top:120px;
            }
            .w {
                width:131px;
                padding:1em;
                position:absolute;
                border: 1px solid black;
                z-index:4;
                border-radius:1em;
                border:1px solid #346789;
                box-shadow: 2px 2px 19px #e0e0e0;
                -o-box-shadow: 2px 2px 19px #e0e0e0;
                -webkit-box-shadow: 2px 2px 19px #e0e0e0;
                -moz-box-shadow: 2px 2px 19px #e0e0e0;
                -moz-border-radius:0.5em;
                border-radius:0.5em;
                opacity:0.8;
                filter:alpha(opacity=80);
                cursor:move;
                height: 35px;
                height: 100px;
            }
        
            .aLabel {
                background-color:white;
                opacity:0.8;
                padding:0.3em;				
            }
        
            .ep {
                float:right;
                width:1em;
                height:1em;
                background-color:#0033cc;
                cursor:pointer;
            }
        
            ._jsPlumb_endpoint {
                z-index:3;
            }
            .dragHover { border:1px dotted red; }
            path { cursor:pointer; }
        ";
        $numNodes = count($nodes);
        for ($i=0; $i<$numNodes; $i++) {
            $left = intdiv($i, $numItemsPerColumn)*195 + 5;
            $top = ($i % $numItemsPerColumn-1)*145 + 130;
            echo "#" . $nodes[$i]['id'] . " {
                        left: " . $left . "px;
                        top: " . $top . "px;
                }";                    
        };
    }

    function stateMachineDemoJS($nodes) {
        $counter = 0;
        $connections = array();
        for ($i=0; $i<count($nodes); $i++) {
            if ($nodes[$i]['targets']!='') {
                if (strpos($nodes[$i]['targets'], ",")>0) {
                    for ($j=0; $j<count(explode(",", $nodes[$i]['targets'])); $j++)
                            $connections[$counter++] = array($nodes[$i]['id'], explode(",", $nodes[$i]['targets'])[$j]);
                } else {
                    $connections[$counter++] = array($nodes[$i]['id'], $nodes[$i]['targets']);
                }
            }
        }
        echo '
            $("#alx_all").html("' . count($connections) . '");
            numConnections=' . count($connections) . ';
            numCorrectConnections=0;
            ;(function() {
                var curColourIndex = 1, maxColourIndex = 24, nextColour = function() {
                    var R,G,B;
                    R = parseInt(128+Math.sin((curColourIndex*3+0)*1.3)*128);
                    G = parseInt(128+Math.sin((curColourIndex*3+1)*1.3)*128);
                    B = parseInt(128+Math.sin((curColourIndex*3+2)*1.3)*128);
                    curColourIndex = curColourIndex + 1;
                    if (curColourIndex > maxColourIndex) curColourIndex = 1;
                    return "rgb(" + R + "," + G + "," + B + ")";
                };
                    
                window.jsPlumbDemo = { 
                    init :function() {
                        jsPlumb.importDefaults({
                            Endpoint : ["Dot", {radius:2}],
                            HoverPaintStyle : {strokeStyle:"#42a62c", lineWidth:2 },
                            ConnectionOverlays : [
                                [ "Arrow", { 
                                    location:1,
                                    id:"arrow",
                                    length:14,
                                    foldback:0.8
                                } ],
                                [ "Label", { label:" ", id:"label" }] 
                            ]
                        });
            
                        jsPlumb.draggable(jsPlumb.getSelector(".w"));
                        jsPlumb.bind("click", function(c) { 
                            jsPlumb.detach(c);
                            numCorrectConnections--;
                            $("#alx_correct").html(numCorrectConnections);
                        });
                            
                        jsPlumbDemo.initEndpoints(nextColour);
                        jsPlumb.bind("jsPlumbConnection", function(conn) {
                            conn.connection.setPaintStyle({strokeStyle:nextColour()});
                        });
            
                        jsPlumb.makeTarget(jsPlumb.getSelector(".w"), {
                            dropOptions:{ hoverClass:"dragHover" },
                            anchor:"Continuous"';
                            
        if (count($connections)>0) {
            echo        ',       
                            beforeDrop:function(params) { 
                                if (';
            for ($i=0; $i<(count($connections)-1); $i++) {
                echo '( params.sourceId == "' . $connections[$i][0] . '" &&  params.targetId == "' . $connections[$i][1] . '" ) || ';
            } 	
            echo '( params.sourceId == "' . $connections[count($connections)-1][0] . '" &&  params.targetId == "' . $connections[count($connections)-1][1] . '" )';                                                   
            echo             '){
                                    numCorrectConnections++;
                                    $("#alx_correct").html(numCorrectConnections);
                                    if (numCorrectConnections==numConnections) {
                                        $("#explanation").html("<img src=\"images/winner.gif\">");
                                    }
                                    return true;
                                } else {
                                    return false;
                                }
                            }'; 
        }
            echo               '});
                    }
                };
            })();';
    }
?>